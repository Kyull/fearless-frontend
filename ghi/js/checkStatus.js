// Get the cookie out of the cookie store

const payloadCookie = await cookieStore.get("jwt_access_payload")
if (payloadCookie) {
  // The cookie value is a JSON-formatted string, so parse it
  const encodedPayload = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
  const decodedPayload = atob(encodedPayload);

  // The payload is a JSON-formatted string, so parse it
  const payload = JSON.parse(decodedPayload);

  // Print the payload
  console.log(payload);

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
  // for (let perm of payload.user.perms){
  //   if (perm === "events.add_conference"){
  //       console.log(perm)
  //       const logged1 = document.querySelectorAll('ul > li > a')
  //       console.log(logged1)
  //       logged1.classList.remove('d-none')
  //   }
  // }
  const permissions = payload.user.perms;
  if (permissions.includes("events.add_conference")) {
    const link = document.querySelector("[href='new-conference.html']");
    if (link) {
      link.classList.remove('d-none');
    }
  }
  
  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
  if (permissions.includes("events.add_location")) {
    const link = document.querySelector("[href='new-location.html']");
    if (link) {
      link.classList.remove('d-none');
    }
  }
}

  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
