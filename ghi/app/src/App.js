import Nav from './Nav'
import MainPage from './MainPage';
import PresentationForm from './PresentationForm';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import { Route, Routes, BrowserRouter} from 'react-router-dom';


function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path='/presentations/new' element={<PresentationForm />} />
          <Route path='/conferences/new' element={<ConferenceForm />} />
          <Route path='/locations/new' element={<LocationForm />} />
          <Route path="attendees">
            <Route index element={<AttendeesList />} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>

          {/* <AttendeesList attendees={props.attendees} /> */}
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
