import { NavLink } from "react-router-dom";

function Nav() {
    return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" href="/">Conference GO!</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink to="/" className="nav-link" aria-current="page" href="/">Home</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/locations/new" className="nav-link" id="logged-in-link-1" aria-current="page" href="/locations/new">New location</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/conferences/new" className="nav-link" id="logged-in-link-2" aria-current="page" href="/conferences/new">New conference</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/presentations/new" className="nav-link" id="logged-in-link-3" aria-current="page" href="/presentations/new">New presentation</NavLink>
                            </li>
                            <li className="nav-item">
                                <NavLink to="/attendees" className="nav-link" id="logged-in-link-2" aria-current="page" href="/attendees">Attendees</NavLink>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
    );
}

export default Nav;
